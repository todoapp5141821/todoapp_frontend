import React, { useState, useEffect } from "react";
import { Navigate } from "react-router-dom";

export const Login = () => {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [errors, setErrors] = useState('');
    const [token, setToken] = useState('');
    const [user, setUser] = useState('');



    const data = {
        "email" : email,
        "password" : pass
    }

    async function postData(url, data) {
  
        const response = await fetch(url, {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          credentials: "same-origin",
          headers: {
            "Content-Type": "application/json",
          },
          redirect: "follow",
          referrerPolicy: "no-referrer",
          body: JSON.stringify(data),
        });
        return response.json();
      }
       

      const handleSubmit = (e) => {
        e.preventDefault();
        postData("http://todo-prod-1210822924.eu-west-1.elb.amazonaws.com/api/login", data ).then((data) => {
          setErrors(data);
          setToken(data.token);
          setUser(data.email)
          
      });
    }

    useEffect(() => {
      localStorage.setItem('token', token);
    }, [token]);
    useEffect(() => {
      localStorage.setItem('user', user);
    }, [user]);

    

    if(token !== undefined && token.length > 0) {
      return <Navigate to= "/todo/create" />
    }

    
    
    return (
        <div className="auth-form-container">
            <form className="login-form" onSubmit={handleSubmit}>
                <h3>{errors.password} {errors.errorMessage}</h3>
                <label htmlFor="email">email</label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="email@gmail.com" id ="email" name="email"/>
                <label htmlFor="password">password</label>
                <input value={pass} onChange={(e) => setPass(e.target.value)} type="password" placeholder="*********" id="password" name="password"></input>
                <button type="submit">Log In</button>
            </form>
            <a href="/user/create">Don't have an account? Register here</a>
        </div>
    )
}