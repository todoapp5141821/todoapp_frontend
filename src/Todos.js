import { useState, useEffect } from 'react';
import { Navigate } from "react-router-dom";
import './App.css'

function Todos() {

  const [todos, setTodos] = useState([]);
  const [token, setToken] = useState('');

    useEffect(() => {
      setToken(localStorage.getItem('token'));
    }, [token]);

  useEffect(() => {

  let token = localStorage.getItem('token');

  async function getData(url) {
  
    const response = await fetch(url, {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
        Authorization: 'Bearer ' + token
      },
      redirect: "follow",
      referrerPolicy: "no-referrer",
    });
    return response.json();
  }

    if(!token) return;

    getData("http://todo-prod-1210822924.eu-west-1.elb.amazonaws.com/api/todo").then((data) => {
      setTodos(data);
      console.log(data);
    })

  }, []);

  const logOut = (e) => {
    e.preventDefault();
    localStorage.setItem('token', undefined);
    setToken(undefined)
  }
  if (token === undefined) {
    return <Navigate to= "/user/login" />
  }
  
  return (
   <>
    <ul>
      {todos.map((item,index)=>{
        return <li key={index}>{item.title}, deadline - {item.deadline}</li>
      })}
    </ul>

    <a href='/todo/create'>Add todo</a>
    <form onSubmit={logOut}>
        <button type="submit">Log out</button>
    </form>
   </>
  );
}

export default Todos;