import React, { useState, useEffect } from "react"
import { Navigate } from "react-router-dom";

export const CreateTodo = () => {
    const [title, setTitle] = useState('');
    const [deadline, setDeadline] = useState('');
    const [token, setToken] = useState('');
    const [user, setUser] = useState('');

    useEffect(() => {
      setToken(localStorage.getItem('token', token));
    }, [token]);
    useEffect(() => {
      setUser(localStorage.getItem('user', user));
    }, [user]);


    const data = {
        "title" : title,
        "deadline" : deadline
    }

    async function postData(url, data) {
  
        const response = await fetch(url, {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          credentials: "same-origin",
          headers: {
            "Content-Type": "application/json",
            Authorization: 'Bearer ' + token
          },
          redirect: "follow",
          referrerPolicy: "no-referrer",
          body: JSON.stringify(data),
        });
        return response.json();
      }
      
      

    const handleSubmit = (e) => {
        e.preventDefault();
        postData("http://todo-prod-1210822924.eu-west-1.elb.amazonaws.com/api/todo", data ).then((data) => {
        console.log(data);
      });
    }

    const logOut = (e) => {
      e.preventDefault();
      localStorage.setItem('token', undefined)
      setToken(undefined);
    }
    if (token === undefined) {
      return <Navigate to= "/user/login" />
    }

    return (
        <div className="todo-form-container">
            <h3>Hello {user}</h3>
            <form className="todo-form" onSubmit={handleSubmit}>
                <label htmlFor="title">title</label>
                <input value={title} onChange={(e) => setTitle(e.target.value)} type="text" placeholder="title" id ="title" name="title"/>
                <label htmlFor="deadline">password</label>
                <input value={deadline} onChange={(e) => setDeadline(e.target.value)} type="date" placeholder="deadline" id="deadline" name="deadline"></input>
                <button type="submit">Add</button>
            </form>
            <a href="/todo/all">See all todos</a>
            <form onSubmit={logOut}>
              <button type="submit">Log out</button>
            </form>
        </div>
    )
}