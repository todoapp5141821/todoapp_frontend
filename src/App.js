import './App.css';
import {    
  Routes,  
  Route
}   
from 'react-router-dom';  

import Hello from './Hello';
import Test from './Test';
import { Register } from './Register';
import { Login } from './Login';
import { CreateTodo } from './CreateTodo';
import Todos from './Todos';



function App() {
  return (
    
    <Routes>
      <Route exact path='/hello-world' element={ <Hello /> } />
      <Route path='/user/create' element={ <Register /> }/>
      <Route path='/user/login' element={ <Login /> }/>
      <Route path='/todo/create' element={ <CreateTodo /> }/>
      <Route path='/todo/all' element={ <Todos /> }/>
      <Route path='/' element="ok"/>
      <Route exact path='/test' element={ <Test /> } />
    </Routes>

  );
}

export default App;
