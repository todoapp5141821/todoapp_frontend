import React, { useState } from "react";
import { Navigate } from "react-router-dom";

export const Register = () => {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [confirmPass, setconfirmPass] = useState('');
    const [errors, setErrors] = useState('');
    const [user, setUser] = useState('');

    const data = {
        "email" : email,
        "password" : pass,
        "confirmPass" : confirmPass
    }

    async function postData(url , data) {
  
        const response = await fetch(url, {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          credentials: "same-origin",
          headers: {
            "Content-Type": "application/json",
          },
          redirect: "follow",
          referrerPolicy: "no-referrer",
          body: JSON.stringify(data),
        });
        return response.json();
      }
      
      

    const handleSubmit = (e) => {
        e.preventDefault();
        postData("http://todo-prod-1210822924.eu-west-1.elb.amazonaws.com/api/register", data ).then((data) => {
          setErrors(data);
          setUser(data.registeredUser)
          
      });
    }

    if(user.length > 0) {
      return <Navigate to= "/user/login" />
    }

    return (
        <div className="auth-form-container">
            <form className="register-form" onSubmit={handleSubmit}>
                <h3>{errors.email}</h3>
                <label htmlFor="email">email</label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="email@gmail.com" id ="email" name="email"/>
                <h3>{errors.password} {errors.errorMessage}</h3>
                <label htmlFor="password">password</label>
                <input value={pass} onChange={(e) => setPass(e.target.value)} type="password" placeholder="*********" id="password" name="password"></input>
                <h3>{errors.confirmPass}</h3>
                <label htmlFor="confirmPass">confirm password</label>
                <input value={confirmPass} onChange={(e) => setconfirmPass(e.target.value)} type="password" placeholder="*********" id="confirmPass" name="confirmPass"></input>
                <button type="submit">Register</button>
            </form>
        <a href="/user/login">Already have an account? Log in here</a>
    </div>
    )
}