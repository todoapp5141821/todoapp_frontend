import { render, screen } from '@testing-library/react';
import Hello  from './Hello';

test('should have text content of "Hello World', () => {
  render ( <Hello /> );
  const helloH2 = screen.queryByTestId("hello");
  expect(helloH2.innerHTML).toEqual("Hello World");
});
