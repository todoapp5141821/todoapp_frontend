import { useState } from 'react';
import './App.css'

function Test() {

  const [test, setHealth] = useState();

  fetch('http://todo-prod-1210822924.eu-west-1.elb.amazonaws.com/api/test-cors')
  .then(response => response.json())
  .then(data => setHealth(data.test));

  return (
   <>
    <h2>test cors: {test}</h2>
   </>
  );
}

export default Test;