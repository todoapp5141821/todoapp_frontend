FROM node:18-alpine
WORKDIR /to-do-app
ENV PATH=".node_modules/.bin:$PATH"
COPY . .
RUN npm i react-scripts
RUN npm run build
EXPOSE 3000

CMD ["npm", "start"]